unit main;
{$ifdef FPC}{$mode objfpc}{$h+}{$endif}
interface
uses
 mseglob,mseguiglob,mseguiintf,mseapplication,msestat,msemenus,msegui,
 msegraphics,msegraphutils,mseevent,mseclasses,mseforms,msegraphedits,
 mseificomp,mseificompglob,mseifiglob,msescrollbar,msetypes,msesimplewidgets,
 msewidgets,msedispwidgets,mserichstring,msestrings;

type
 tmainfo = class(tmainform)
   a: tbooleanedit;
   b: tbooleanedit;
   c: tbooleanedit;
   d: tbooleanedit;
   e: tbooleanedit;
   antw1: tbooleanedit;
   antw2: tbooleanedit;
   antw3: tbooleanedit;
   antw4: tbooleanedit;
   antw5: tbooleanedit;
   pruefen: tbutton;
   richtig: tintegerdisp;
   falsch: tintegerdisp;
   procedure dataentered(const sender: TObject);
   procedure pruefenexe(const sender: TObject);
 end;
var
 mainfo: tmainfo;
implementation
uses
 main_mfm;

procedure tmainfo.dataentered(const sender: TObject);
begin
 antw1.color:= cl_default; //farben zuruecksetzen
 antw2.color:= cl_default;
 antw3.color:= cl_default;
 antw4.color:= cl_default;
 antw5.color:= cl_default;
end;

procedure tmainfo.pruefenexe(const sender: TObject);
var
 richtigzahl: integer;
 
 procedure vergleicheantwort(soll: boolean; ist: tbooleanedit);
 begin
  if ist.value = soll then begin //antworten ueberpruefen
   ist.color:= cl_green;         //richtig
   richtigzahl:= richtigzahl + 1;
  end
  else begin
   ist.color:= cl_red;           //falsch
  end;
 end;

const
 fragenzahl = 5;
var
 erg1,erg2,erg3,erg4,erg5: boolean;
begin
             //ergebnisse berechnen
 erg1:= a.value and b.value and c.value and d.value or e.value;
 erg2:= not a.value and not (b.value or c.value) and not (d.value and e.value);
 erg3:= not (e.value or a.value) and (a.value and b.value and 
                                                    not (c.value or d.value));
 erg4:= a.value or b.value or c.value or d.value or e.value;
 erg5:= not (not a.value and not b.value and not c.value and 
                                                 not d.value and not e.value);
 richtigzahl:= 0; //initialisieren
 vergleicheantwort(erg1,antw1);
 vergleicheantwort(erg2,antw2);
 vergleicheantwort(erg3,antw3);
 vergleicheantwort(erg4,antw4);
 vergleicheantwort(erg5,antw5);
 richtig.value:= richtigzahl;        //ergebnisse anzeigen   
 falsch.value:= fragenzahl - richtigzahl;
end;

end.
