unit main;
{$ifdef FPC}{$mode objfpc}{$h+}{$endif}
interface
uses
 mseglob,mseguiglob,mseguiintf,mseapplication,msestat,msemenus,msegui,
 msegraphics,msegraphutils,mseevent,mseclasses,mseforms,msegraphedits,
 mseificomp,mseificompglob,mseifiglob,msescrollbar,msetypes,msesimplewidgets,
 msewidgets;

type
 tmainfo = class(tmainform)
   a: tbooleanedit;
   b: tbooleanedit;
   c: tbooleanedit;
   d: tbooleanedit;
   e: tbooleanedit;
   antw1: tbooleanedit;
   antw2: tbooleanedit;
   antw3: tbooleanedit;
   antw4: tbooleanedit;
   antw5: tbooleanedit;
   pruefen: tbutton;
   procedure dataentered(const sender: TObject);
   procedure pruefenexe(const sender: TObject);
 end;
var
 mainfo: tmainfo;
implementation
uses
 main_mfm;
procedure tmainfo.dataentered(const sender: TObject);
begin
end;

procedure tmainfo.pruefenexe(const sender: TObject);
begin
end;

end.
