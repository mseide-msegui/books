unit main;
{$ifdef FPC}{$mode objfpc}{$h+}{$endif}
interface
uses
 mseglob,mseguiglob,mseguiintf,mseapplication,msestat,msemenus,msegui,
 msegraphics,msegraphutils,mseevent,mseclasses,mseforms,msegraphedits,
 mseificomp,mseificompglob,mseifiglob,msescrollbar,msetypes,msesimplewidgets,
 msewidgets;

type
 tmainfo = class(tmainform)
   a: tbooleanedit;
   b: tbooleanedit;
   c: tbooleanedit;
   d: tbooleanedit;
   e: tbooleanedit;
   antw1: tbooleanedit;
   antw2: tbooleanedit;
   antw3: tbooleanedit;
   antw4: tbooleanedit;
   antw5: tbooleanedit;
   pruefen: tbutton;
   procedure dataentered(const sender: TObject);
   procedure pruefenexe(const sender: TObject);
 end;
var
 mainfo: tmainfo;
implementation
uses
 main_mfm;

procedure tmainfo.dataentered(const sender: TObject);
begin
 antw1.color:= cl_default; //farben zuruecksetzen
 antw2.color:= cl_default;
 antw3.color:= cl_default;
 antw4.color:= cl_default;
 antw5.color:= cl_default;
end;

procedure tmainfo.pruefenexe(const sender: TObject);
var
 erg1,erg2,erg3,erg4,erg5: boolean;
begin
             //ergebnisse berechnen
 erg1:= a.value and b.value and c.value and d.value or e.value;
 erg2:= not a.value and not (b.value or c.value) and not (d.value and e.value);
 erg3:= not (e.value or a.value) and (a.value and b.value and 
                                                    not (c.value or d.value));
 erg4:= a.value or b.value or c.value or d.value or e.value;
 erg5:= not (not a.value and not b.value and not c.value and 
                                                 not d.value and not e.value);
                                                 
 if antw1.value = erg1 then begin //antworten ueberpruefen
  antw1.color:= cl_green;         //richtig
 end
 else begin
  antw1.color:= cl_red;           //falsch
 end;
 if antw2.value = erg2 then begin
  antw2.color:= cl_green;
 end
 else begin
  antw2.color:= cl_red;
 end;
 if antw3.value = erg3 then begin
  antw3.color:= cl_green;
 end
 else begin
  antw3.color:= cl_red;
 end;
 if antw4.value = erg4 then begin
  antw4.color:= cl_green;
 end
 else begin
  antw4.color:= cl_red;
 end;
 if antw5.value = erg5 then begin
  antw5.color:= cl_green;
 end
 else begin
  antw5.color:= cl_red;
 end;
end;

end.
