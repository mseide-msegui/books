unit main;
{$ifdef FPC}{$mode objfpc}{$h+}{$endif}
interface
uses
 mseglob,mseguiglob,mseguiintf,mseapplication,msestat,msemenus,msegui,
 msegraphics,msegraphutils,mseevent,mseclasses,mseforms,msedispwidgets,
 mserichstring,msestrings,msedataedits,mseedit,mseificomp,mseificompglob,
 mseifiglob,msetypes;

type
 tmainfo = class(tmainform)
   ergebnis: tintegerdisp;
   a: tintegeredit;
   b: tintegeredit;
   procedure dataentered(const sender: TObject);
 end;
var
 mainfo: tmainfo;
implementation
uses
 main_mfm;
 
procedure tmainfo.dataentered(const sender: TObject);
begin
 ergebnis.value:= a.value + b.value;
end;

end.
