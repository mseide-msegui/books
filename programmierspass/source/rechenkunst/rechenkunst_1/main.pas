unit main;
{$ifdef FPC}{$mode objfpc}{$h+}{$endif}
interface
uses
 mseglob,mseguiglob,mseguiintf,mseapplication,msestat,msemenus,msegui,
 msegraphics,msegraphutils,mseevent,mseclasses,mseforms,msedispwidgets,
 mserichstring,msestrings,msedataedits,mseedit,mseificomp,mseificompglob,
 mseifiglob,msetypes;

type
 tmainfo = class(tmainform)
   ergebnis: tintegerdisp;
 end;
var
 mainfo: tmainfo;
implementation
uses
 main_mfm;
end.
