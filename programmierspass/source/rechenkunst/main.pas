unit main;
{$ifdef FPC}{$mode objfpc}{$h+}{$endif}
interface
uses
 mseglob,mseguiglob,mseguiintf,mseapplication,msestat,msemenus,msegui,
 msegraphics,msegraphutils,mseevent,mseclasses,mseforms,msedispwidgets,
 mserichstring,msestrings,msedataedits,mseedit,mseificomp,mseificompglob,
 mseifiglob,msetypes,msesimplewidgets,msewidgets;

type
 tmainfo = class(tmainform)
   ergebnis: tintegerdisp;
   a: tintegeredit;
   b: tintegeredit;
   beenden: tbutton;
   bina: tintegerdisp;
   binb: tintegerdisp;
   binergebnis: tintegerdisp;
   test: tbutton;
   testergebnis: tintegerdisp;
   procedure dataentered(const sender: TObject);
   procedure beendenexe(const sender: TObject);
   procedure testexe(const sender: TObject);
 end;
var
 mainfo: tmainfo;
implementation
uses
 main_mfm;
 
procedure tmainfo.dataentered(const sender: TObject);
begin
 ergebnis.value:= a.value + b.value;
 if ergebnis.value < 0 then begin
  ergebnis.font.color:= cl_red;
 end
 else begin
  if ergebnis.value = 0 then begin
   ergebnis.font.color:= cl_green;
  end
  else begin
   ergebnis.font.color:= cl_black;
  end;
 end;
 binergebnis.value:= ergebnis.value; //werte in binaeranzeige kopieren
 bina.value:= a.value;
 binb.value:= b.value;
end;

procedure tmainfo.beendenexe(const sender: TObject);
begin
 application.terminated:= true;
end;

procedure tmainfo.testexe(const sender: TObject);
var
 by1: byte;
begin
 by1:= a.value;
 testergebnis.value:= by1;
end;

end.
