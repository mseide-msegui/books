unit main;
{$ifdef FPC}{$mode objfpc}{$h+}{$endif}
interface
uses
 mseglob,mseguiglob,mseguiintf,mseapplication,msestat,msemenus,msegui,
 msegraphics,msegraphutils,mseevent,mseclasses,mseforms,msesimplewidgets,
 msewidgets,msedataedits,mseedit,mseificomp,mseificompglob,mseifiglob,
 msestrings,msetypes,msedispwidgets,mserichstring;

type
 tmainfo = class(tmainform)
   tbutton1: tbutton;
   a: tintegeredit;
   b: tintegeredit;
   ergebnis: tintegerdisp;
   procedure beendenexe(const sender: TObject);
   procedure dataenteredexe(const sender: TObject);
 end;
var
 mainfo: tmainfo;
implementation
uses
 main_mfm;
 
procedure tmainfo.beendenexe(const sender: TObject);
begin
 application.terminated:= true;
end;

procedure tmainfo.dataenteredexe(const sender: TObject);
begin
 ergebnis.value:= a.value + b.value;
end;

end.
