unit main;
{$ifdef FPC}{$mode objfpc}{$h+}{$endif}
interface
uses
 mseglob,mseguiglob,mseguiintf,mseapplication,msestat,msemenus,msegui,
 msegraphics,msegraphutils,mseevent,mseclasses,mseforms,msesimplewidgets,
 msewidgets,msedispwidgets,mserichstring,msestrings;

type
 tmainfo = class(tmainform)
   schacht: tpaintbox;
   kabine: tpaintbox;
   bedienfeld: tpaintbox;
   posdisp: trealdisp;
   geschwdisp: trealdisp;
   beschdisp: trealdisp;
   procedure createexe(const sender: TObject);
   procedure bedienfeldpaint(const sender: twidget; const acanvas: tcanvas);
   procedure bedienfeldmouseevent(const sender: twidget;
                   var ainfo: mouseeventinfoty);
 end;

var
 mainfo: tmainfo;

implementation

uses
 main_mfm,sysutils;

const
 massstab = 20.0; //pixel pro m
 stockwerkzahl = 11;
 stockwerkhoehe = 2.4;         // m
 kabinenhoehe = 2.0;           // m
 kabinenhoehepix = round(kabinenhoehe*massstab);
 kabinenbreite = 3.0;          // m
 kabinenbreitepix = round(kabinenbreite*massstab);
 schachthoehe = stockwerkzahl * stockwerkhoehe;
 tastenxpix = round((kabinenbreite+0.2)*massstab);
 tastenypix = round(1.5*massstab);
 tastengroessepix = 10;

type
 stockwerkinfoty = record
  bezeichnung: msestring;
  tuereauf: boolean;
  anforderungauf: boolean;
  anforderungab: boolean;
  ziel: boolean; //true wenn ensprechende taste auf bedienfeld gedrueckt
 end;

 aufzuginfoty = record
  stockwerke: array[0..stockwerkzahl-1] of stockwerkinfoty;
 end;

var
 tasteaufrect: rectty;
 tasteabrect: rectty;
 tasteaufpfeil: array[0..2] of pointty;
 tasteabpfeil: array[0..2] of pointty;
 bedienfeldtastenstart: integer;
 bedienfeldtastenrect: rectty;
 bedienfeldtastendistanz: integer;
 aufzug: aufzuginfoty;
 
procedure tmainfo.createexe(const sender: TObject);
var
 i1: integer;
begin
 tasteaufrect.x:= tastenxpix;
 tasteaufrect.y:= -tastenypix; //ursprung ist stockwerkboden
 tasteaufrect.cx:= tastengroessepix;
 tasteaufrect.cy:= tastengroessepix;
 tasteabrect:= tasteaufrect;
 tasteabrect.y:= tasteabrect.y + tastengroessepix + 1;
 tasteaufpfeil[0].x:= tasteaufrect.x + 1;
 tasteaufpfeil[0].y:= tasteaufrect.y + tasteaufrect.cy - 1;
 tasteaufpfeil[1].x:= tasteaufrect.x + tasteaufrect.cx div 2;
 tasteaufpfeil[1].y:= tasteaufrect.y + 1;
 tasteaufpfeil[2].x:= tasteaufrect.x + tasteaufrect.cx - 1;
 tasteaufpfeil[2].y:= tasteaufpfeil[0].y;
 tasteabpfeil:= tasteaufpfeil;
 tasteabpfeil[0].y:= tasteabrect.y + 1;
 tasteabpfeil[1].y:= tasteabrect.y + tasteabrect.cy - 1;
 tasteabpfeil[2].y:= tasteabpfeil[0].y;
 bedienfeldtastenstart:= beschdisp.bounds_y +
                                  beschdisp.bounds_cy + 10;
 bedienfeldtastendistanz:= tasteaufrect.cy+1;
 bedienfeldtastenrect.x:= beschdisp.bounds_x;
 bedienfeldtastenrect.y:= 0;
 bedienfeldtastenrect.size:= tasteaufrect.size; //gleiche groessen

 schacht.clientheight:= round(massstab * schachthoehe);
 kabine.bounds_x:= 1; //linksbuendig, 1 pixel breiter schacht rahmen
 kabine.bounds_cx:= kabinenbreitepix;
 kabine.bounds_cy:= kabinenhoehepix;
 posdisp.value:= 0.0;
 geschwdisp.value:= 0.0;
 beschdisp.value:= 0.0;
 
 with aufzug do begin
  for i1:= 0 to high(stockwerke) do begin
   case i1 of
    0: begin
     stockwerke[i1].bezeichnung:= 'UG';
    end;
    1: begin
     stockwerke[i1].bezeichnung:= 'EG';
    end;
    else begin
     stockwerke[i1].bezeichnung:= inttostr(i1-1); //in unit sysutils
    end;
   end;
  end;
 end;
end;

procedure tmainfo.bedienfeldpaint(const sender: twidget;
                                          const acanvas: tcanvas);

 procedure zeichneetagenknopf(const stockwerkinfo: stockwerkinfoty);
 var
  co1: colorty;
 begin
  if stockwerkinfo.ziel then begin
   co1:= cl_ltyellow;
  end
  else begin
   co1:= cl_dkgray;
  end;
  acanvas.fillrect(bedienfeldtastenrect,co1);
  with bedienfeldtastenrect do begin
   acanvas.drawstring(stockwerkinfo.bezeichnung,mp(x+cx+5,y+cy));
  end;
 end; //zeichneetagenknopf

var
 i1: integer;
begin
 acanvas.move(mp(0,bedienfeldtastenstart));
 with aufzug do begin
  for i1:= high(stockwerke) downto 0 do begin
   zeichneetagenknopf(stockwerke[i1]);
   acanvas.move(mp(0,bedienfeldtastendistanz));
  end;
 end;
end;

procedure tmainfo.bedienfeldmouseevent(const sender: twidget;
                                           var ainfo: mouseeventinfoty);
var
 pt1: pointty;
 i1,i2: integer;
begin
 if ainfo.eventkind = ek_buttonpress then begin
  pt1:= ainfo.pos;
  pt1.y:= pt1.y - bedienfeldtastenstart; 
                                        //oberste taste entspricht null
  i1:= pt1.y div bedienfeldtastendistanz; //anzahl tasten von oben
  i2:= high(aufzug.stockwerke) - i1;          //stockwerknummer
  if (i2 >= 0) and (i2 <= high(aufzug.stockwerke)) then begin //gueltig?
                                                      //ja
   pt1.y:= pt1.y - i1 * bedienfeldtastendistanz;
                   //taste des aktuellen stockwerkes entspricht null
   if pointinrect(pt1,bedienfeldtastenrect) then begin 
                                  //wurde auf taste geklickt?
    aufzug.stockwerke[i2].ziel:= true;   //ja
    bedienfeld.invalidate(); //muss eventuell neu gezeichnet werden
   end;
  end;
 end;
end;

end.
