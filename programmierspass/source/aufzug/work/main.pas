unit main;
{$ifdef FPC}{$mode objfpc}{$h+}{$endif}
interface
uses
 msetypes,mseglob,mseguiglob,mseguiintf,mseapplication,msestat,msemenus,msegui,
 msegraphics,msegraphutils,mseevent,mseclasses,mseforms,msesimplewidgets,
 msewidgets,msedispwidgets,mserichstring,msestrings,msetimer;

type
 tmainfo = class(tmainform)
   schacht: tpaintbox;
   kabine: tpaintbox;
   bedienfeld: tpaintbox;
   posdisp: trealdisp;
   geschwdisp: trealdisp;
   beschdisp: trealdisp;
   tick: ttimer;
   procedure createexe(const sender: TObject);
   procedure bedienfeldpaint(const sender: twidget; const acanvas: tcanvas);
   procedure bedienfeldmouseevent(const sender: twidget;
                   var ainfo: mouseeventinfoty);
   procedure schachtafterpaint(const sender: twidget;
                                             const acanvas: tcanvas);
   procedure schachtmouseevent(const sender: twidget;
                   var ainfo: mouseeventinfoty);
   procedure tickexe(const sender: TObject);
 end;

var
 mainfo: tmainfo;

implementation

uses
 main_mfm,sysutils,msedate;

const
 massstab = 20.0; //pixel pro m
 tickprosekunde = 50;
 stockwerkzahl = 11;
 stockwerkhoehe = 2.4;         // m
 kabinenhoehe = 2.0;           // m
 kabinenhoehepix = round(kabinenhoehe*massstab);
 kabinenbreite = 3.0;          // m
 kabinenbreitepix = round(kabinenbreite*massstab);
 schachthoehe = stockwerkzahl * stockwerkhoehe;
 tastenxpix = round((kabinenbreite+0.2)*massstab);
 tastenypix = round(1.5*massstab);
 tastengroessepix = 10;

type
 stockwerkinfoty = record
  bezeichnung: msestring;
  tuereauf: boolean;
  anforderungauf: boolean;
  anforderungab: boolean;
  ziel: boolean; //true wenn ensprechende taste auf bedienfeld gedrueckt
 end;

 aufzuginfoty = record
  stockwerke: array[0..stockwerkzahl-1] of stockwerkinfoty;
  zielstockwerk: integer;
  zielstockwerkvorher: integer;
  anhaltestockwerk: integer;
  offenetuere: integer; //stockwerk mit offener tuer, -1 fuer keine
  tuereaufzeitpunkt: tdatetime; //zeitpunkt letzte tueroeffnung UTC
  gestartet: boolean;
 end;

var
 tasteaufrect: rectty;
 tasteabrect: rectty;
 tasteaufpfeil: array[0..2] of pointty;
 tasteabpfeil: array[0..2] of pointty;
 bedienfeldtastenstart: integer;
 bedienfeldtastenrect: rectty;
 bedienfeldtastendistanz: integer;
 aufzug: aufzuginfoty;
 
type
 motorinfoty = record
  sollgeschwindigkeit: float; //m/s, negativ -> abwaerts
  endschalteroben: boolean;   //true wenn betätigt
  endschalterunten: boolean;  //true wenn betätigt
  kabinenposition: float;     //m, distanz des kabinenbodens vom schachtboden
 end;

var
 motor: motorinfoty;
  
procedure motortick(var motorinfo: motorinfoty);
begin
 with motorinfo do begin
  if (sollgeschwindigkeit > 0) and not endschalteroben or
                (sollgeschwindigkeit < 0) and not endschalterunten then begin
   kabinenposition:= kabinenposition + sollgeschwindigkeit / tickprosekunde;
  end;
 end;
end;

const
 maximalgeschwindigkeit = 4.0; // m/s
 anhaltegeschwindigkeit = 0.1; // m/s
 maximalbeschleunigung = 1.0;  // m/s^2

type
 regelinfoty = record
  sollposition: float;    // m
  geschwindigkeit: float; // m/s
 end;

var
 regelung: regelinfoty;
 
procedure regelungtick(var regelinfo: regelinfoty; var motorinfo: motorinfoty);
var
 distanz: float;            // m
 anhaltezeit: float;        // s
 anhalteweg: float;         // m
 beschleunigung: float;     // m/s^2
 absgeschwindigkeit: float; // m/s absolutgeschwindigkeit, immer positiv
begin
 distanz:= regelinfo.sollposition - motorinfo.kabinenposition;
 absgeschwindigkeit:= abs(regelinfo.geschwindigkeit);
 if (distanz > 0.01) or (distanz < -0.01) or 
               (absgeschwindigkeit > anhaltegeschwindigkeit) then begin
                //position nicht erreicht oder zu schnell ->
                //neue geschwindigkeit berechnen
  anhaltezeit:= absgeschwindigkeit / maximalbeschleunigung;
  anhalteweg:= regelinfo.geschwindigkeit * anhaltezeit / 2.0;
  beschleunigung:= maximalbeschleunigung;
  if (anhalteweg >= distanz) xor (distanz < 0.0) then begin 
                                //bremsen notwendig?
   beschleunigung:= -maximalbeschleunigung; //ja, bremsen
  end;
  if distanz < 0 then begin
   beschleunigung:= -beschleunigung; //umgekehre richtung
  end;
  regelinfo.geschwindigkeit:= regelinfo.geschwindigkeit + 
           beschleunigung/tickprosekunde; //beschleunigen oder bremsen
  if regelinfo.geschwindigkeit > maximalgeschwindigkeit then begin
                             //steiggeschwindigkeit begrenzen
   regelinfo.geschwindigkeit:= maximalgeschwindigkeit;
  end;
  if regelinfo.geschwindigkeit < -maximalgeschwindigkeit then begin
                             //sinkgeschwindigkeit begrenzen
   regelinfo.geschwindigkeit:= -maximalgeschwindigkeit;
  end;
 end
 else begin
  regelinfo.geschwindigkeit:= 0.0; // position erreicht, stop
 end;
 motorinfo.sollgeschwindigkeit:= regelinfo.geschwindigkeit;
end;

const
 tuereaufzeit = 5.0/(24.0*60*60); //5s
 
function steuerung(var aufzug: aufzuginfoty; 
                         var regelinfo: regelinfoty): boolean;
                              //true wenn aenderungen vorgenommen wurden

 procedure oeffnetuere(); //oeffnet tuere des zielstockwerkes
 begin
  with aufzug do begin
   offenetuere:= zielstockwerk;
   tuereaufzeitpunkt:= nowutc();
   result:= not stockwerke[zielstockwerk].tuereauf; //anderung vorgenommen
   if result then begin
    guibeep();    //akustisches signal
   end;
   stockwerke[zielstockwerk].tuereauf:= true;
  end;
 end; //oeffnetuere

var
 i1: integer;
begin
 result:= false; //init
 with aufzug do begin
  if gestartet then begin
   if (regelinfo.geschwindigkeit = 0) then begin //ziel erreicht
    anhaltestockwerk:= zielstockwerk;
    gestartet:= false;
    oeffnetuere();
    with stockwerke[zielstockwerk] do begin
     anforderungauf:= false;
     anforderungab:= false;
     ziel:= false;
    end;
    result:= true; //aenderung vorgenommen
   end;
  end
  else begin //nicht gestartet
   for i1:= 0 to stockwerkzahl-1 do begin
    with stockwerke[i1] do begin
     if anforderungauf or ziel then begin
      if not gestartet and (anhaltestockwerk = i1) then begin
       anforderungauf:= false; //keine anforderung notwendig, sofort ruecksetzen
       ziel:= false;
       oeffnetuere();
      end;
      zielstockwerk:= i1;
     end;
     if anforderungab or ziel then begin
      if not gestartet and (anhaltestockwerk = i1) then begin
       anforderungab:= false; //keine anforderung notwendig, sofort ruecksetzen
       ziel:= false;
       oeffnetuere();
      end;
      zielstockwerk:= i1;
     end;
     if (i1 = offenetuere) and tuereauf and 
           (nowutc() - tuereaufzeitpunkt > tuereaufzeit) then begin
      tuereauf:= false;
      result:= true; //aenderungen vorgenommen
     end;
    end;
   end;
   if zielstockwerk <> zielstockwerkvorher then begin
    if not gestartet and not stockwerke[zielstockwerkvorher].tuereauf then begin
                       //fahrt kann gestartet werden
     gestartet:= true;
     zielstockwerkvorher:= zielstockwerk;
     regelinfo.sollposition:= zielstockwerk * stockwerkhoehe;
    end;
   end;
  end;
 end;
end;

procedure visualisierung(schachtgeaendert: boolean);
begin
 mainfo.kabine.bounds_y:= round((schachthoehe - motor.kabinenposition - 
                                               kabinenhoehe) * massstab) + 1;
                       //1 pixel schacht frame
 motor.endschalterunten:= motor.kabinenposition <= 0.0;
 motor.endschalteroben:= motor.kabinenposition + kabinenhoehe >= schachthoehe;
 mainfo.posdisp.value:= motor.kabinenposition;
 mainfo.beschdisp.value:= 
         (motor.sollgeschwindigkeit - mainfo.geschwdisp.value) * tickprosekunde;
 mainfo.geschwdisp.value:= motor.sollgeschwindigkeit;
 if schachtgeaendert then begin
  mainfo.schacht.invalidate(); //muss neu gezeichnet werden
  mainfo.bedienfeld.invalidate();
 end;
end;

procedure tmainfo.createexe(const sender: TObject);
var
 i1: integer;
begin
 tasteaufrect.x:= tastenxpix;
 tasteaufrect.y:= -tastenypix; //ursprung ist stockwerkboden
 tasteaufrect.cx:= tastengroessepix;
 tasteaufrect.cy:= tastengroessepix;
 tasteabrect:= tasteaufrect;
 tasteabrect.y:= tasteabrect.y + tastengroessepix + 1;
 tasteaufpfeil[0].x:= tasteaufrect.x + 1;
 tasteaufpfeil[0].y:= tasteaufrect.y + tasteaufrect.cy - 1;
 tasteaufpfeil[1].x:= tasteaufrect.x + tasteaufrect.cx div 2;
 tasteaufpfeil[1].y:= tasteaufrect.y + 1;
 tasteaufpfeil[2].x:= tasteaufrect.x + tasteaufrect.cx - 1;
 tasteaufpfeil[2].y:= tasteaufpfeil[0].y;
 tasteabpfeil:= tasteaufpfeil;
 tasteabpfeil[0].y:= tasteabrect.y + 1;
 tasteabpfeil[1].y:= tasteabrect.y + tasteabrect.cy - 1;
 tasteabpfeil[2].y:= tasteabpfeil[0].y;
 bedienfeldtastenstart:= beschdisp.bounds_y +
                                  beschdisp.bounds_cy + 10;
 bedienfeldtastendistanz:= tasteaufrect.cy+1;
 bedienfeldtastenrect.x:= beschdisp.bounds_x;
 bedienfeldtastenrect.y:= 0;
 bedienfeldtastenrect.size:= tasteaufrect.size; //gleiche groessen

 schacht.clientheight:= round(massstab * schachthoehe);
 kabine.bounds_x:= 1; //linksbuendig, 1 pixel breiter schacht rahmen
 kabine.bounds_cx:= kabinenbreitepix;
 kabine.bounds_cy:= kabinenhoehepix;
 posdisp.value:= 0.0;
 geschwdisp.value:= 0.0;
 beschdisp.value:= 0.0;
 
 with aufzug do begin
  for i1:= 0 to high(stockwerke) do begin
   case i1 of
    0: begin
     stockwerke[i1].bezeichnung:= 'UG';
    end;
    1: begin
     stockwerke[i1].bezeichnung:= 'EG';
    end;
    else begin
     stockwerke[i1].bezeichnung:= inttostr(i1-1); //in unit sysutils
    end;
   end;
  end;
 end;
 motor.kabinenposition:= 0.0;     //zuunterst
 motor.sollgeschwindigkeit:= 0.0; //aus
 regelung.sollposition:= 0.0;
 regelung.geschwindigkeit:= 0.0;  //aus;
 tick.interval:= 1000000 div tickprosekunde;
 tick.enabled:= true;
 aufzug.offenetuere:= -1; //alle tueren geschlossen
end;

procedure tmainfo.bedienfeldpaint(const sender: twidget;
                                          const acanvas: tcanvas);

 procedure zeichneetagenknopf(const stockwerkinfo: stockwerkinfoty);
 var
  co1: colorty;
 begin
  if stockwerkinfo.ziel then begin
   co1:= cl_ltyellow;
  end
  else begin
   co1:= cl_dkgray;
  end;
  acanvas.fillrect(bedienfeldtastenrect,co1);
  with bedienfeldtastenrect do begin
   acanvas.drawstring(stockwerkinfo.bezeichnung,mp(x+cx+5,y+cy));
  end;
 end; //zeichneetagenknopf

var
 i1: integer;
begin
 acanvas.move(mp(0,bedienfeldtastenstart));
 with aufzug do begin
  for i1:= high(stockwerke) downto 0 do begin
   zeichneetagenknopf(stockwerke[i1]);
   acanvas.move(mp(0,bedienfeldtastendistanz));
  end;
 end;
end;

procedure tmainfo.bedienfeldmouseevent(const sender: twidget;
                                           var ainfo: mouseeventinfoty);
var
 pt1: pointty;
 i1,i2: integer;
begin
 if ainfo.eventkind = ek_buttonpress then begin
  pt1:= ainfo.pos;
  pt1.y:= pt1.y - bedienfeldtastenstart; 
                                        //oberste taste entspricht null
  i1:= pt1.y div bedienfeldtastendistanz; //anzahl tasten von oben
  i2:= high(aufzug.stockwerke) - i1;          //stockwerknummer
  if (i2 >= 0) and (i2 <= high(aufzug.stockwerke)) then begin //gueltig?
                                                      //ja
   pt1.y:= pt1.y - i1 * bedienfeldtastendistanz;
                   //taste des aktuellen stockwerkes entspricht null
   if pointinrect(pt1,bedienfeldtastenrect) then begin 
                                  //wurde auf taste geklickt?
    aufzug.stockwerke[i2].ziel:= true;   //ja
    bedienfeld.invalidate(); //muss eventuell neu gezeichnet werden
   end;
  end;
 end;
end;

procedure tmainfo.schachtafterpaint(const sender: twidget;
                                             const acanvas: tcanvas);

 procedure zeichnestockwerk(const stockwerkinfo: stockwerkinfoty;
                         erstes: boolean; letztes: boolean);
 var
  co1: colorty;
 begin
  acanvas.drawline(mp(0,0),mp(schacht.bounds_cx,0)); //boden
  acanvas.drawline(mp(0,-kabinenhoehepix),
                          mp(schacht.bounds_cx,-kabinenhoehepix)); //decke
  acanvas.drawline(mp(kabinenbreitepix,-kabinenhoehepix),
                                    mp(kabinenbreitepix,0),cl_dkgray); //rechts
  if stockwerkinfo.tuereauf then begin
   acanvas.drawline(mp(2,-kabinenhoehepix),mp(2,0)); //links
   acanvas.drawline(mp(kabinenbreitepix-2,-kabinenhoehepix),
                    mp(kabinenbreitepix-2,0));       //rechts
  end
  else begin
   acanvas.drawline(mp(kabinenbreitepix div 2,-kabinenhoehepix),
                    mp(kabinenbreitepix div 2,0));   //mitte
  end;
  if not letztes then begin
   acanvas.fillrect(tasteaufrect,cl_gray);
   if stockwerkinfo.anforderungauf then begin
    co1:= cl_ltyellow;
   end
   else begin
    co1:= cl_black;
   end;
   acanvas.fillpolygon(tasteaufpfeil,co1);
  end;
  if not erstes then begin
   acanvas.fillrect(tasteabrect,cl_gray);
   if stockwerkinfo.anforderungab then begin
    co1:= cl_ltyellow;
   end
   else begin
    co1:= cl_black;
   end;
   acanvas.fillpolygon(tasteabpfeil,co1);
  end;
  acanvas.drawstring(stockwerkinfo.bezeichnung,
                mp(tasteabrect.x+tasteabrect.cx+5,tasteabrect.y));
         //etagen beschriftung
 end;
 
var
 i1,i2: integer;
 schiebung: integer;
begin
                      //y richtung = von oben nach unten
 acanvas.move(mp(0,round(stockwerkhoehe*stockwerkzahl*massstab)));
                            //ursprung = erdgeschossboden
 schiebung:= 0;       //summe der ursprungverschiebung
 for i1:= 0 to stockwerkzahl - 1 do begin
  i2:= round(i1 * stockwerkhoehe * massstab) - schiebung;
                            //notwendige ursprungsverschiebung
  acanvas.move(mp(0,-i2));   //aufwaertsverschiebung
  schiebung:= schiebung + i2; //summe nachfuehren
  zeichnestockwerk(aufzug.stockwerke[i1],i1=0,i1=stockwerkzahl-1);
 end;
end;

procedure tmainfo.schachtmouseevent(const sender: twidget;
               var ainfo: mouseeventinfoty);
var
 stockwerk: integer;
 pt1: pointty;
begin
 if ainfo.eventkind = ek_buttonpress then begin
  pt1:= ainfo.pos;
  stockwerk:= trunc(((mainfo.schacht.clientheight-pt1.y) / massstab) / 
                                                           stockwerkhoehe);
  if (stockwerk >= 0) and (stockwerk < stockwerkzahl) then begin
   pt1.y:= pt1.y - round((stockwerkzahl-stockwerk)*stockwerkhoehe*massstab);
   if pointinrect(pt1,tasteaufrect) then begin
    aufzug.stockwerke[stockwerk].anforderungauf:= true;
   end;
   if pointinrect(pt1,tasteabrect) then begin
    aufzug.stockwerke[stockwerk].anforderungab:= true;
   end;
   schacht.invalidate(); //koennte veraendert sein
  end;
 end;
end;

procedure tmainfo.tickexe(const sender: TObject);
var
 schachtgeaendert: boolean;
begin
 schachtgeaendert:= steuerung(aufzug,regelung);
 regelungtick(regelung,motor);
 motortick(motor);
 visualisierung(schachtgeaendert);
end;

end.
