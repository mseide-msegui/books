unit main;
{$ifdef FPC}{$mode objfpc}{$h+}{$endif}
interface
uses
 msetypes,mseglob,mseguiglob,mseguiintf,mseapplication,msestat,msemenus,msegui,
 msegraphics,msegraphutils,mseevent,mseclasses,mseforms,msedataedits,mseedit,
 mseificomp,mseificompglob,mseifiglob,msestrings,msedispwidgets,mserichstring;

type
 tmainfo = class(tmainform)
   eingabe: tintegeredit;
   zifferdisp: tstringdisp;
   wortedisp: tstringdisp;
   ziffernformatdisp: tstringdisp;
   procedure setvalueexe(const sender: TObject; var avalue: Integer;
                   var accept: Boolean);
 end;
var
 mainfo: tmainfo;

implementation
uses
 main_mfm,msewidgets;

type
 zifferty = 0..9;
 
function zifferzuzeichen(ziffer: zifferty): msechar;
begin
 result:= msechar(ord('0') + ziffer);
end;

function zifferzuwort(ziffer: zifferty): msestring;
begin
 case ziffer of
  0: begin
   result:= 'null';
  end;
  1: begin
   result:= 'eins';
  end;
  2: begin
   result:= 'zwei';
  end;
  3: begin
   result:= 'drei';
  end;
  4: begin
   result:= 'vier';
  end;
  5: begin
   result:= 'fünf';
  end;
  6: begin
   result:= 'sechs';
  end;
  7: begin
   result:= 'sieben';
  end;
  8: begin
   result:= 'acht';
  end;
  9: begin
   result:= 'neun';
  end;
 end;
end;

procedure tmainfo.setvalueexe(const sender: TObject; var avalue: Integer;
                                                         var accept: Boolean);
var
 ziffern: msestring;
 ziffernformatiert: msestring;
 worte: msestring;
 wert: integer;
 zehnerwert: zifferty;
 i1: integer;
 
begin
 if avalue <= 0 then begin
  showmessage('Ungültiger Betrag!');
  accept:= false;
 end
 else begin
  wert:= avalue;
  ziffern:= '';                //initialisieren
  ziffernformatiert:= '';
  worte:= '';
  repeat
   zehnerwert:= wert mod 10;
   ziffern:= ziffern + zifferzuzeichen(zehnerwert); //reihenfolge umgekehrt
   worte:= zifferzuwort(zehnerwert) + '-' + worte;  //reihenfolge richtig
   wert:= wert div 10;
  until wert = 0;
  setlength(worte,length(worte)-1); //letztes '-' abtrennen 
  for i1:= length(ziffern) downto 1 do begin
   if (i1 mod 3 = 0) and (i1 <> length(ziffern)) then begin
    ziffernformatiert:= ziffernformatiert + ' ';
                //jede dritte ziffer ein leerzeichen einfuegen
   end;
   ziffernformatiert:= ziffernformatiert + ziffern[i1];
                //reihenfolge umkehren
  end;
  zifferdisp.value:= ziffern;
  ziffernformatdisp.value:= ziffernformatiert;
  wortedisp.value:= worte;
 end;
end;

end.
