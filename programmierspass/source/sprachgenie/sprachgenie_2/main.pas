unit main;
{$ifdef FPC}{$mode objfpc}{$h+}{$endif}
interface
uses
 msetypes,mseglob,mseguiglob,mseguiintf,mseapplication,msestat,msemenus,msegui,
 msegraphics,msegraphutils,mseevent,mseclasses,msewidgets,mseforms,msedataedits,
 mseedit,mseificomp,mseificompglob,mseifiglob,msestrings;

type
 tmainfo = class(tmainform)
   eingabe: tintegeredit;
 end;
var
 mainfo: tmainfo;
implementation
uses
 main_mfm;
end.
