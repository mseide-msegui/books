unit main;
{$ifdef FPC}{$mode objfpc}{$h+}{$endif}
interface
uses
 mseglob,mseguiglob,mseguiintf,mseapplication,msestat,msemenus,msegui,
 msegraphics,msegraphutils,mseevent,mseclasses,mseforms,msesimplewidgets,
 msewidgets,msetypes,msetimer;

type
 tmainfo = class(tmainform)
   timer: ttimer;
   start: tbutton;
   stop: tbutton;
   reset: tbutton;
   panorama: tpaintbox;
   facetemplate: tfacecomp;
   procedure timerexe(const sender: TObject);
   procedure startexe(const sender: TObject);
   procedure stopexe(const sender: TObject);
   procedure resetexe(const sender: TObject);
   procedure paintexe(const sender: twidget; const acanvas: tcanvas);
 end;
var
 mainfo: tmainfo;
implementation
uses
 main_mfm;

const
 panoramaschritt = 2; //pixel pro panoramapunkt
 panoramabreite = 300 div panoramaschritt; //anzahl panoramapunkte
 panoramahoehe = 200; //pixel
 erosionsgeschwindigkeit = 5; //pixel pro schritt

type
 panoramavektorty = array[0..panoramabreite - 1] of float;
 
var
 hoehe: panoramavektorty;
 erosion: panoramavektorty;

procedure rueckstellen();
var
 i1: integer;
begin
 for i1:= 0 to high(hoehe) do begin
  hoehe[i1]:= 0;
 end;
 mainfo.panorama.invalidate();
end;

procedure tmainfo.timerexe(const sender: TObject);
var
 i1: integer;
begin
 for i1:= 0 to high(hoehe) do begin
  hoehe[i1]:= hoehe[i1] + erosion[i1]; //wachstums und erosionswirkung
 end;
 panorama.invalidate;                 //panorama muss neue gezeichnet werden
end;

procedure glaetten(durchschnittlaenge: integer; 
                const quelle: panoramavektorty; out ziel: panoramavektorty);
var
 i1: integer;
 durchschnitt: float;
begin
 if durchschnittlaenge > panoramabreite then begin
  halt(1); //fataler fehler, programm muss mit fehler nummer abgebrochen werden
 end;
 durchschnitt:= 0.0;
 for i1:= 0 to durchschnittlaenge - 1 do begin //bilde startwert
  durchschnitt:= durchschnitt + quelle[i1];
 end;
 for i1:= 0 to high(quelle) do begin
  ziel[i1]:= durchschnitt/durchschnittlaenge; 
              //erhält den durschnittswert der folgenden werte 
  durchschnitt:= durchschnitt - quelle[i1]; //letzten wert entfernen
  durchschnitt:= durchschnitt + quelle[(i1+durchschnittlaenge) mod 
                                                          panoramabreite];
                              //neuen wert hinzufügen, kreis schliessen
 end;
end;
 
procedure tmainfo.startexe(const sender: TObject);

const
 durchschnittzahl = 128;
 glaettungszahl = 5;
var
 zufall: panoramavektorty;
 erosionroh: panoramavektorty;
 i1: integer;
begin
 for i1:= 0 to high(zufall) do begin
  zufall[i1]:= erosionsgeschwindigkeit*(2*random() - 1); 
                    //zufallszahl im bereich -1..+1
 end;
 glaetten(durchschnittzahl,zufall,erosionroh);
 glaetten(glaettungszahl,erosionroh,erosion);
 timer.enabled:= true;
end;

procedure tmainfo.stopexe(const sender: TObject);
begin
 timer.enabled:= false;
end;

procedure tmainfo.resetexe(const sender: TObject);
begin
 rueckstellen();
end;

procedure tmainfo.paintexe(const sender: twidget; const acanvas: tcanvas);
const
 verschiebung = panoramahoehe div 3 ;
var
 panoramapixel: array[0..panoramabreite-1] of pointty;
 i1: integer;
begin
 for i1:= 0 to high(panoramapixel) do begin
  panoramapixel[i1].x:= i1 * panoramaschritt;
  panoramapixel[i1].y:= verschiebung + round(hoehe[i1]);
 end;
 acanvas.color:= cl_red;
 acanvas.drawlines(panoramapixel);
end;

end.
