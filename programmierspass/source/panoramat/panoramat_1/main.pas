unit main;
{$ifdef FPC}{$mode objfpc}{$h+}{$endif}
interface
uses
 mseglob,mseguiglob,mseguiintf,mseapplication,msestat,msemenus,msegui,
 msegraphics,msegraphutils,mseevent,mseclasses,mseforms,msesimplewidgets,
 msewidgets,msetypes,msetimer;

type
 tmainfo = class(tmainform)
   timer: ttimer;
   start: tbutton;
   stop: tbutton;
   reset: tbutton;
   procedure timerexe(const sender: TObject);
   procedure startexe(const sender: TObject);
   procedure stopexe(const sender: TObject);
   procedure resetexe(const sender: TObject);
 end;
var
 mainfo: tmainfo;
implementation
uses
 main_mfm;

const
 panoramaschritt = 2; //pixel pro panoramapunkt
 panoramabreite = 300 div panoramaschritt; //anzahl panoramapunkte
 panoramahoehe = 200; //pixel
 erosionsgeschwindigkeit = 0.05; //pixel pro schritt

type
 panoramavektorty = array[0..panoramabreite - 1] of float;
 
var
 hoehe: panoramavektorty;
 erosion: panoramavektorty;

procedure tmainfo.timerexe(const sender: TObject);
begin
 guibeep();
end;

procedure tmainfo.startexe(const sender: TObject);
begin
 timer.enabled:= true;
end;

procedure tmainfo.stopexe(const sender: TObject);
begin
 timer.enabled:= false;
end;

procedure tmainfo.resetexe(const sender: TObject);
begin
end;

end.
